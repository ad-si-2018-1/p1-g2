// Load the TCP Library
net = require('net');

// Keep track of the chat clients
var clients = [];

//Mapa de Nicks
var nicks = {};

var password = [];

var channels = [];



// Start a TCP Server
net.createServer(function (socket) {

  // Identify this client
  socket.name = socket.remoteAddress + ":" + socket.remotePort 

  // Put this new client in the list
  clients.push(socket);

  // Send a nice welcome message and announce
  socket.write("Welcome " + socket.name + "\n");
  broadcast(socket.name + " joined the chat\n", socket);

  // Handle incoming messages from clients.
  socket.on('data', function (data) {
    //broadcast(socket.name + "> " + data, socket);
    analisar(data);
  });

  // Remove the client from the list when it leaves
  socket.on('end', function () {
    clients.splice(clients.indexOf(socket), 1);
    broadcast(socket.name + " left the chat.\n");
    
  });
  
  // Send a message to all clients
  function broadcast(message, sender) {
    clients.forEach(function (client) {
      // Don't want to send it to sender
      if (client === sender) return;
      client.write(message);
    });
    // Log it to the server output too
    process.stdout.write(message)
  }

  function analisar(data){
      
    var mensagem = String(data).trim();
    var args = mensagem.split(" ");

    if (args[0] == "NICK") nick(args);
    else if( args[0] == "USER") user(args);
    else if ( args[0] == "JOIN") join(args);
    else if (args[0] == "QUIT") quit(args);
    else if (args[0] == "LIST") list(args);
    else socket.write("Erro: comando inexistente\n");
  }
  function nick(args){

    if ( ! args[1]){
      socket.write("ERRO: nickname faltando\n");
      return;
    }
    else if (nicks [ args[1]]){
      socket.write("ERRO: nickname já existe\n");
      return;
    }
    else {
      if (socket.nick){
        delete nicks[socket.nick];
      }
      nicks [args[1]] = socket.name;

      socket.nick = args[1];
    }

    socket.write("Ok: comando NICK executado com sucesso\n");

  } 
  function quit(args) { 
		
		
		texto = "\nO usuario " + socket.nick + " saiu!\n";
		broadcast (texto);
		
		delete socket.nick;
		delete socket.name;
		
		socket.end();                                

  }
  function list(args) {
    // <channel> *( "," <channel> ) [ <target> ]
    var ch;
    for (ch in channels)
        socket.write (channels[ch]);
}
  function join(args) {
    socket.channel = args[1];
    socket.write("Canal: " + args[1]);
    broadcast(socket.name + "entrou na sala " + args[1], socket);
    channels = channels + socket.channel;
    socket.write("\nOK: comando JOIN executado com sucesso\n");
}

  function user(args){
    socket.write("Ok: comando USER executado com sucesso\n");
  }
}).listen(6667);

// Put a friendly message on the terminal of the server.
console.log("Chat server running at port 6667\n");