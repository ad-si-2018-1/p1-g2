# Introdução

O presente trabalho tem como objetivo criar um servidor IRC utilizando sockets em nodeJS, para a disciplina de Aplicações distrubuídas 2018/1 oeferecido pelo Universidade Federal de Goiás. O IRC é um protocolo utilizado na Internet utilizado para teleconferência, foi escrito pelo programador finlandês Jarkko Oikarinen em 1988 na Universidade de Oulu na Finlândia. Ele é constituído de de servidores e clientes, de maneira que cada servidor contém informações sobre todo o sistema (clientes, usuários, outros servidores, etc.). O protocolo IRC é um protocolo baseado em texto, com o cliente mais simples sendo qualquer programa de soquete capaz de se conectar ao servidor. Esta comunicação ocorre sobre TCP/IP, utilizando o protocolo TCP na camada de rede. O IRC  é baseado em troca de mensagens constituídas de até 512 octetos (caracteres ASCII) e está descrito no RFC 1459. O RFC é um documento que descreve os padrões de cada protocolo da Internet a serem considerados um padrão.


# Arquitetura Cliente-servidor

A arquitetura utilizada para implementação do protocolo IRC é o cliente-servidor, é uma estrutura de aplicação distribuída que distribui as tarefas e cargas de trabalho entre os fornecedores de um recurso ou serviço, designados como servidores, e os requerentes dos serviços, designados como clientes. Clientes e servidores se comunicam através de rede de computadores, estando em computadores disintos ou no mesmo. 

# Clientes 

Um cliente é qualquer conexão a um servidor, que não seja um servidor. Eles são diferenciados através de um apelido (ou nickname) único com tamanho máximo de nove caracteres. Além disso, todos os servidores devem ter as seguintes informações sobre os clientes: o host do cliente, o seu username e o nome do servidor ao qual este está conectado.

# Servidores 

Os Servidores formam o backbone do IRC e são os pontos de conexão para outros servidores e pra clientes. A configuração de uma rede IRC assemelha-se a uma spanning tree, onde cada servidor age como um nodo central para o resto da rede que ele está conectado.

# Mensagens

No IRC as mensagens são constituídas de três partes, separadas por espaços: 

### prefixo (opcional)
    O prefixo é utilizado em algumas situações para indicar quem é o servidor ou cliente de origem.
### comando
    É uma string ou conjunto de três números que indica um comando ou uma resposta numérica.
### parâmetros de comando
    Até 15 argumentos que podem ser enviados durante as mensagens. O último argumento se iniciado por ':' pode conter espaços.

# Comandos 

O procolo IRC tem vários comandos, listamos alguns dos mais usados:

## Comandos para registro de conexão

PASS:
Utilizado para definir uma senha de conexão.

NICK:
Utilizado para dar a um usuário um nickname ou para mudá-lo.

USER:
Serve para especificar dados do cliente para o servidor no início de uma conexão.

SERVER:
Esta mensagem diz a um servidor que no outro lado da nova conexão está outro servidor.

OPER:
Comando utilizado por um usuário normal para obter privilégios de operador.

QUIT:
Uma sessão de cliente é terminada com uma mensagem deste tipo.

SQUIT:
Este comando é necessário para informar sobre servidores que estão deixando a rede IRC.
Comandos para operações com canais

JOIN:
Comando usado por um cliente para começar a escutar um canal específico.

PART:
Remove um cliente de um canal.

MODE:
Altera o modo de usuários e canais.

TOPIC:
Altera o tópico de um canal.

NAMES:
Lista todos os nicknames em um determinado canal.

LIST:
Informa todos os canais e seus tópicos.

INVITE:
 Comando utilizado para convidar usuários para entrar em um canal.

KICK:
Remove um usuário de um canal.

## Comandos e Consultas de Servidores

VERSION:
Retorna a versão do servidor.

STATS:
Mostra estatísticas do servidor indicado.

LINKS:
Permite com que o usuário liste todos os servidores que são conhecidos por determinado servidor.

TIME:
Mostra a hora local de um servidor.

CONNECT:
Estabelece uma conexão entre dois servidores.

TRACE:
O comando TRACE é utilizado para achar rotas a um servidor específico.

ADMIN:
    Usado para descobrir um administrador de um servidor.

INFO:
    Retorna informações que descrevem um servidor.
## Comandos de envio de mensagens

PRIVMSG: 
    Envia uma mensagem entre dois usuários.

NOTICE:
    Envia uma mensagem para um canal.
    Consultas de Usuários

WHO:
    Utilizado por um cliente para retornar informações sobre clientes, baseados em uma máscara.

WHOIS:
    Retorna informações específicas sobre um cliente.

WHOWAS:
    Informa sobre um nickname que não existe mais.
## Outros Comandos

KILL:
    Fecha uma conexão cliente-servidor.

PING:
    Testar se um cliente está ativo.

PONG:
    Resposta a uma mensagem com PING.

ERROR:
    Usado pelos servidores para reportar um erro sério ou fatal.



