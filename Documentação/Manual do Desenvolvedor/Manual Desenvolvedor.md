# Introdução

Esse documento tem como objetivo auxiliar o desenvolvedor a configurar o ambiente de desenvolvimento. Foram utilizadas diversas ferramentas para implementação do IRC, primeiro para controle de versão e para repositório dos códigos o documentações, utilizamos o GitLab, abaixo temos o tutorial de como configirar o mesmo. A linguagem utilizada foi NodeJS. Utilizamos máquinas virtuais com SO linux para melhor manuseio dos comando, a plataforma utilizada foi a VirtualBox. 

# GitLab 

GitLab é um gerenciador de repositório Git para Web, ele também dispõem Wiki e rastreamento de Issues.

## Instalação (Ubunto)

Habilite o acesso HTTP e SSH no firewall do sistema

#sudo apt-get install curl policycoreutils openssh-server openssh-clients

>#systemctl enable sshd

>#systemctl start sshd

>#sudo apt-get install postfix

>#systemctl enable postfix

>#systemctl start postfix

## Configuração do Firewall

>#firewall-cmd --permanent --add-service=http

>#firewall-cmd --permanent --add-service=https

>#systemctl reload firewalld 

Adicionar o repositório Yum

#curl -sS https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | bash

Instalar o Gitlab

>#yum install gitlab-ce

>#gitlab-ctl reconfigure

## Configurações iniciais do Git

>$git config --global user.name "user.name.gitlab"

>$git config --global user.email "email@provedor.com.br"

>$git config --global core.editor vim

>$git config --global merge.tool vimdiff

>$git config --global color.ui true

## Clone
O comando “git clone” vai copiar o projeto do Gitlab para sua máquina.

>$git clone https://gitlab.com/ad-si-2018-1/p1-g2 

>$cd p1-g2
## arquivo README.md
Criar o arquivo de README.md, ele não é obrigatório mas muito recomendado.

>$touch README.md

>$vi README.md
 
>$cat README.md

>Projeto 1 apicações distribuídas .

>Commit 1.

>Teste Git 

## Aicionando o README.md

>$git add README.md 

## Commit 

O comando “git commit” vai gravar as alterações no repositório local.
git commit -m “comentário”

>$ git commit -m "teste"

O commando "git push", sincroniza o repositório local com o Gitlab. 
 
 >$git push -u origin master


## Criar um novo branch
Cada projeto tem um branch master, e neste branch master podem ser anexados vários branchs paralelos.
>$git clone https://gitlab.com/ad-si-2018-1/p1-g2 

>$cd p1-g2

>$ git branch teste

>$ git checkout teste

>$ git status 

## Sincronização com o branch "teste"

>$git push -u origin teste

## Merge 

Após criado o Branch de teste e sincronizar, falta sincronizar o branch de teste com o branch master. São 3 comandos: git checkout, git merge e git push.

>$git checkout master
 
>$git merge teste
 
>$git push origin master

# Node.JS

Primeiramente precisamos de um editor de texto, que pode utilizamos o SublimeText, tendo um editor de texto, vamos instalar o Node.JS, geralmente instalamos somente como o comando $sudo apt-get install -y nodejs porém tivemos alguns conflitos e então fizemos alguns modificações na intalção, onde instalamos o NVM primeiro e depois o Node.

>$sudo apt-get update sudo apt-get install build-essential libssl-dev
>$curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh _-o installnvm.sh

Execute o scrpit com:

>_bash installnvm.sh

Depois execute

>$nvm ls-remote

Escolha a versão do node e instale:

>$nvm install 7.3.0

A configuração do Node.JS já está pronta. Agora só abrir o editor de texto e salvar os arquivos com a extensão .js 

# Alguns comandos utilizados

## join 

Comando utilizado para incluir algum cliente no canal 

    function join(args) {
        socket.channel = args[1];
        socket.write("Canal: " + args[1]);
        broadcast(socket.name + "entrou na sala " + args[1], socket);
        channels = channels + socket.channel;ß
    socket.write("\nOK: comando JOIN executado com sucesso\n");
}

## list 

Esse comando envia uma lista de canais

    function list(args) {
            // <channel> *( "," <channel> ) [ <target> ]
            // mautec: como ainda nao temos topicos para a sala
            // o comando apenas lista as salas existentes
            var ch;
    for (ch in channels)
        socket.write (channels[ch] + '\n');
}


## QUIT

Comando para excluir um cliente do canal

    function quit(args) { 
		
		
		    texto = "\nO usuario " + socket.nick + " saiu!\n";
		    broadcast (texto);
		
		    delete socket.nick;
		    delete socket.name;
		
		    socket.end();      

## broadcast

Essa função envia mensagem para todos os integrantes do canal, porém não envia para quem mandou a mensagem

         function broadcast(message, sender) {
            clients.forEach(function (client) {
            // Don't want to send it to sender
            if (client === sender) return;
            client.write(message);
        });


