# Caso de uso descritivo

1. Os usuários do canal, podem participar, conversar e sair de um canal.
2. As mensagens enviadas para um canal serão replicadas para todos os usuários que estiverem no canal
3. Os usuários conectados a um servidor IRC podem se associar a canais existentes.
